 
# Lab1 NodeJS

## Installation :
 
  

On est sous Archlinux. Nodejs était déjà installé donc je ne me souviens plus très bien de la procédure si ce n’est que suite à un problème concernant un fichier inexistant dans une librairie en suivant le wiki officiel archlinux ( même libraire que mentionné ici : « https://stackoverflow.com/questions/37180758/error-installing-nodejs-in-arch-linux » mais avec un fichier plus récent, mettre à jour la libraire ne résolvait pas le problème), j’avais procédé à l’installation de nodejs + NPM en utilisant NVM (dont la procédure est également dans le wiki officiel archlinux).

## Utilisation :

Pour lancer le serveur, il faut se placer dans le dossier du projet et utiliser la commande :

    nodemon index.js

J'utilise nodemon pour ne pas avoir à relancer le serveur après chaque modification de fichier.
Ensuite on accède au site via :
http://localhost:8080/
ce qui affichera le reste des instructions

On peut essayer des http://localhost:8080/hello?name=jean ou http://localhost:8080/hello?name=Sebastien
Pour afficher des Hello + le prénom

## Contributeur :

**PINTO Stanislas**
**BOUCHERIT Samy**
