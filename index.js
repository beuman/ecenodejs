
// ./index.js
const http = require('http')
const handles = require('./handles')
const server = http.createServer(handles.serverHandle);
server.listen(8080)





const content = '<!DOCTYPE html>' +
'<html>' +
'    <head>' +
'        <meta charset="utf-8" />' +
'        <title>ECE AST</title>' +
'    </head>' + 
'    <body>' +
'         <p>Heththttodrld</p>' +
'    </body>' +
'</html>'



const serverHandle = function (req, res) {
//parse la route
  const route = url.parse(req.url)
  //on récupère le path
  const path = route.pathname 
  //récupèr ele paramètre
  const params = qs.parse(route.query)

  res.writeHead(200, {'Content-Type': 'text/plain'});

  if (path === '/hello' && 'name' in params) {
    res.write('Hello ' + params['name'])
  } else {
    res.write('Hello anonymous')
  }

  res.end();

  }

  // Declare an http server
  const server = http.createServer(serverHandle);


 // Start the server
  server.listen(8080)


  
/* ANCIEN CODE V3



// Import a module
const http = require('http')
// Import Node url module
const url = require('url')
const qs = require('querystring')



const content = '<!DOCTYPE html>' +
'<html>' +
'    <head>' +
'        <meta charset="utf-8" />' +
'        <title>ECE AST</title>' +
'    </head>' + 
'    <body>' +
'         <p>Heththttodrld</p>' +
'    </body>' +
'</html>'



const serverHandle = function (req, res) {
//parse la route
  const route = url.parse(req.url)
  //on récupère le path
  const path = route.pathname 
  //récupèr ele paramètre
  const params = qs.parse(route.query)

  res.writeHead(200, {'Content-Type': 'text/plain'});

  if (path === '/hello' && 'name' in params) {
    res.write('Hello ' + params['name'])
  } else {
    res.write('Hello anonymous')
  }

  res.end();

  }

  // Declare an http server
  const server = http.createServer(serverHandle);

  // Start the server
  server.listen(8080)






ANCIEN CODE V2:
// Import a module
const http = require('http')
// Import Node url module
const url = require('url')


const content = '<!DOCTYPE html>' +
'<html>' +
'    <head>' +
'        <meta charset="utf-8" />' +
'        <title>ECE AST</title>' +
'    </head>' + 
'    <body>' +
'         <p>Hello Wyjjodrld !</p>' +
'    </body>' +
'</html>'



const serverHandle = function (req, res) {

    // Retrieve and print the current path
  const path = url.parse(req.url).pathname;
  console.log(path);


    // Write a response header
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(content);
    res.write(path);
    res.end();
  }


*/

/* ANCIEN CODE :

// Declare an http server
http.createServer(function (req, res) {

  // Write a response header
  res.writeHead(200, {'Content-Type': 'text/plain'});

  // Write a response content
  res.end('Hello World\n');

// Start the server
}).listen(8080)

// curl localhost:8080 or go to http://localhost:8080

*/
